import pandas as pd
from pprint import pprint

data = ['Tyumen', 'Tobolsk', 'Moskow', 'London', 'New York']
indexes = ['a', 'b', 'c', 'd', 'e']

values = [1, 2, 3, 4, 5, 6, 7, 8, 9]
vs = pd.Series(values)

data_series = pd.Series(data, index=indexes)

# pprint(values * 2)
# pprint(vs ** 2)

df = pd.DataFrame(
    {
        'Cities': data_series,
        'Countries': ['Russia', 'Russia', 'Russia', 'UK', 'Usa'],
        'Count': [700000, 120000, 8000000, 10000000, 8600000]
    },
    #index=['a', 'b', 'c', 'd', 'e']
)

#new_df = df[df['Count'] > 7000000]
#pprint(new_df)

pprint(df.loc['a'].count)

df = pd.read_csv('./df.csv')

pprint(df)